package stormnet.interfaces;

public interface Squareable {
    double getSquare();
}
