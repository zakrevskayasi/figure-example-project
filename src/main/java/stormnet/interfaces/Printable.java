package stormnet.interfaces;

public interface Printable {
    void print();
}
