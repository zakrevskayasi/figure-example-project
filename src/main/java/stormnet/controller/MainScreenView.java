package stormnet.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import stormnet.figure.*;
import stormnet.interfaces.Printable;
import stormnet.interfaces.Squareable;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenView implements Initializable {
    @FXML
    private Canvas canvas;
    private Figure[] figures;
    private Random rnd;

    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        rnd = new Random(System.currentTimeMillis());
    }

    private Figure createFigure(Point point) {
        Figure figure = null;
        int randLineWidth = rnd.nextInt(10);
        int lineWidth = randLineWidth < 5 ? 5 : randLineWidth;
        switch (rnd.nextInt(3)) {
            case Figure.FIGURE_TYPE_RECTANGLE:
                double sideA = rnd.nextInt(101);
                double sideB = rnd.nextInt(101);
                figure = new Rectange(lineWidth, Color.GREEN, point, sideB < 10 ? 10 : sideB, sideA < 10 ? 10 : sideA);
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                double base = rnd.nextInt(101);
                figure = new Triangle(lineWidth, Color.BLUE, point, base < 10 ? 10 : base);
                break;
            case Figure.FIGURE_TYPE_CIRCLE:
                int radius = rnd.nextInt(51);
                figure = new Circle(lineWidth, Color.RED, point, radius < 10 ? 10 : radius);
                break;
            default:
                System.out.println("Unknown figure type!");
        }
        return figure;
    }

    private void addFigure(Figure figure) {
        if(figure == null){
            return;
        }

        if(figures[figures.length - 1] == null){
            figures[figures.length - 1] = figure;
        }else {
            Figure[] tmp = new Figure[figures.length + 1];
            int index = 0;
            for( ; index < figures.length; index++){
                tmp[index] = figures[index];
            }
            tmp[index] = figure;
            figures = tmp;
        }
    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0,0,1024,600);
        for(Figure figure : figures){
            if(figure != null){
                figure.draw(canvas.getGraphicsContext2D());
            }
        }
    }

    @FXML
    private void canvasClicked(MouseEvent mouseEvent) {
        System.out.println("X: " + mouseEvent.getX() + "\nY: " + mouseEvent.getY());
        addFigure(createFigure(new Point(mouseEvent.getX(), mouseEvent.getY())));
        repaint();
        printFiguresInfo(figures);
        printOverallSquare(figures);
    }

    public void printOverallSquare(Squareable[] objs){
        double overallSquare = 0.0;
        for (Squareable s : objs){
            if(s != null){
                overallSquare += s.getSquare();
            }
        }
        System.out.println("Overall square: " + overallSquare);
    }

    public void printFiguresInfo(Printable[] objs){
        for (Printable pr: objs){
            if(pr != null){
                pr.print();
            }
        }
    }
}
