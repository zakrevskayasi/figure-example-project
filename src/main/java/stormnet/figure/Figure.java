package stormnet.figure;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import stormnet.interfaces.Printable;
import stormnet.interfaces.Squareable;

public abstract class Figure implements Printable, Squareable {
    public static final int FIGURE_TYPE_RECTANGLE = 0;
    public static final int FIGURE_TYPE_TRIANGLE = 1;
    public static final int FIGURE_TYPE_CIRCLE = 2;

    private int type;

    protected double lineWidth;
    protected Color color;
    protected Point center;

    public Figure(int type, double lineWidth, Color color, Point center) {
        this.type = type;
        this.lineWidth = lineWidth;
        this.color = color;
        this.center = center;
    }

    public int getType() {
        return type;
    }

    public Color getColor() {
        return color;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public Point getCenter() {
        return center;
    }

    public double square() {
        return 0.0;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public abstract void draw(GraphicsContext gc);

    public void print() {
        System.out.println(toString());
    }
}
