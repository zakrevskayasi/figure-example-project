package stormnet.figure;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {
    private double radius;

    private Circle(double lineWidth, Color color, Point center) {
        super(FIGURE_TYPE_CIRCLE, lineWidth, color, center);
    }

    public Circle(double lineWidth, Color color, Point center, double radius) {
        this(lineWidth, color, center);
        this.radius = radius;
    }

    public void draw(GraphicsContext graphicsContext){
        graphicsContext.setLineWidth(lineWidth);
        graphicsContext.setStroke(color);
        graphicsContext.strokeOval(center.x - radius, center.y - radius, radius * 2, radius * 2);
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append(", center=").append(center);
        sb.append('}');
        return sb.toString();
    }

    public double getSquare() {
        return Math.PI * Math.pow(radius, 2);
    }
}
