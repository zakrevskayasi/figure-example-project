package stormnet.figure;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static java.lang.Math.sqrt;

public class Rectange extends Figure {
    private double a;
    private double b;

    private Rectange(double lineWidth, Color color, Point center) {
        super(FIGURE_TYPE_RECTANGLE, lineWidth, color, center);
    }

    public Rectange(double lineWidth, Color color, Point center, double a, double b) {
        this(lineWidth, color, center);
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getB() {
        return b;
    }



    public void draw(GraphicsContext graphicsContext){
        graphicsContext.setLineWidth(lineWidth);
        graphicsContext.setStroke(color);
        graphicsContext.strokeRect(center.x - a / 2, center.y - b / 2, a, b);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectange{");
        sb.append("a=").append(a);
        sb.append(", b=").append(b);
        sb.append(", center=").append(center);
        sb.append('}');
        return sb.toString();
    }

    public double getSquare() {
        return a * b;
    }
}
