package stormnet.figure;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import static java.lang.Math.*;

public class Triangle extends Figure {
    private double base;

    private Triangle(double lineWidth, Color color, Point center) {
        super(FIGURE_TYPE_TRIANGLE, lineWidth, color, center);
    }

    public Triangle(double lineWidth, Color color, Point center, double base) {
        this(lineWidth, color, center);
        this.base = base;
    }

    public void draw(GraphicsContext graphicsContext){
        double[] arrOfX = {center.x - base / 2, center.x, center.x + base/2};
        double[] arrOfY = {center.y + base * sqrt(3) / 2, center.y - base/sqrt(3), center.y + base * sqrt(3) / 2};
        graphicsContext.setLineWidth(lineWidth);
        graphicsContext.setStroke(color);
        graphicsContext.strokePolygon(arrOfX, arrOfY, 3);
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getBase() {
        return base;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("base=").append(base);
        sb.append(", center=").append(center);
        sb.append('}');
        return sb.toString();
    }

    public double getSquare() {
        return (base * base) / 2;
    }

}
